/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.file.worker.listener;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;


/**
 * @author M-IST
 */
@Component
public class BatchJobListener extends JobExecutionListenerSupport
{
  @Override
  public void afterJob(JobExecution jobExecution)
  {
    if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
      System.out.println("File processing is finished");
    }
  }
}
