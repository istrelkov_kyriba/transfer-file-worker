/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.file.worker.writer;

import com.kyriba.transfer.file.worker.dto.TransferDTO;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author M-IST
 */
@Component
public class TransferWriter implements ItemWriter<TransferDTO>
{
  @Autowired
  private KafkaTemplate<Long, TransferDTO> kafkaTemplate;

  @Value(value = "${kafka.topic.name}")
  private String topicName;

  @Value(value = "${min.garbage.characters}")
  private int minGarbageCharacters;

  @Value(value = "${max.garbage.characters}")
  private int maxGarbageCharacters;

  @Override
  public void write(List<? extends TransferDTO> transfers) throws Exception
  {
    transfers.forEach(transfer -> {
      ProducerRecord record = new ProducerRecord(topicName, System.currentTimeMillis(), transfer);
      if (minGarbageCharacters > 0) {
        record.headers().add(new RecordHeader("garbage",
            "!".repeat(minGarbageCharacters + ((int) (Math.random() * (maxGarbageCharacters - minGarbageCharacters)))).getBytes()));
      }
      kafkaTemplate.send(record);
    });
  }
}