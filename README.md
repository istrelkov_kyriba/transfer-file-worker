# README #

Transfer file worker

## Transfer file worker is gradle project 
- The list of supported commands can be found here https://docs.gradle.org/current/userguide/command_line_interface.html
- E.g. to order to build project you need run: gradle build

## To order to build and load docker image to repository run the following commands:

- docker build -t transfer-file-worker-0.0.1 .
- docker tag transfer-file-worker-0.0.1 kyribastrelkov/transfer-file-worker
- docker push kyribastrelkov/transfer-file-worker